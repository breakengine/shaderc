#include "break/shaderc/Compiler.h"
#include "break/shaderc/Cpp_Utils.h"

#include <glslang/Public/ShaderLang.h>
#include <SPIRV/GlslangToSpv.h>

#include <spirv_hlsl.hpp>

#include <mn/Buf.h>
#include <mn/IO.h>

#include <vector>
#include <assert.h>

using namespace mn;

inline static TBuiltInResource
default_builtin_resources()
{
	TBuiltInResource self{};
	self.maxLights = 32;
	self.maxClipPlanes = 6;
	self.maxTextureUnits = 32;
	self.maxTextureCoords = 64;
	self.maxVertexAttribs = 64;
	self.maxVertexTextureImageUnits = 32,
	self.maxCombinedTextureImageUnits = 80,
	self.maxTextureImageUnits = 32,
	self.maxFragmentUniformComponents = 4096,
	self.maxDrawBuffers = 32,
	self.maxVertexUniformVectors = 128,
	self.maxVaryingVectors = 8,
	self.maxFragmentUniformVectors = 16,
	self.maxVertexOutputVectors = 16,
	self.maxFragmentInputVectors = 15,
	self.minProgramTexelOffset = -8,
	self.maxProgramTexelOffset = 7,
	self.maxClipDistances = 8,
	self.maxComputeWorkGroupCountX = 65535,
	self.maxComputeWorkGroupCountY = 65535,
	self.maxComputeWorkGroupCountZ = 65535,
	self.maxComputeWorkGroupSizeX = 1024,
	self.maxComputeWorkGroupSizeY = 1024,
	self.maxComputeWorkGroupSizeZ = 64,
	self.maxComputeUniformComponents = 1024,
	self.maxComputeTextureImageUnits = 16,
	self.maxComputeImageUniforms = 8,
	self.maxComputeAtomicCounters = 8,
	self.maxComputeAtomicCounterBuffers = 1,
	self.maxVaryingComponents = 60,
	self.maxVertexOutputComponents = 64,
	self.maxGeometryInputComponents = 64,
	self.maxGeometryOutputComponents = 128,
	self.maxFragmentInputComponents = 128,
	self.maxImageUnits = 8,
	self.maxCombinedImageUnitsAndFragmentOutputs = 8,
	self.maxCombinedShaderOutputResources = 8,
	self.maxImageSamples = 0,
	self.maxVertexImageUniforms = 0,
	self.maxTessControlImageUniforms = 0,
	self.maxTessEvaluationImageUniforms = 0,
	self.maxGeometryImageUniforms = 0,
	self.maxFragmentImageUniforms = 8,
	self.maxCombinedImageUniforms = 8,
	self.maxGeometryTextureImageUnits = 16,
	self.maxGeometryOutputVertices = 256,
	self.maxGeometryTotalOutputComponents = 1024,
	self.maxGeometryUniformComponents = 1024,
	self.maxGeometryVaryingComponents = 64,
	self.maxTessControlInputComponents = 128,
	self.maxTessControlOutputComponents = 128,
	self.maxTessControlTextureImageUnits = 16,
	self.maxTessControlUniformComponents = 1024,
	self.maxTessControlTotalOutputComponents = 4096,
	self.maxTessEvaluationInputComponents = 128,
	self.maxTessEvaluationOutputComponents = 128,
	self.maxTessEvaluationTextureImageUnits = 16,
	self.maxTessEvaluationUniformComponents = 1024,
	self.maxTessPatchComponents = 120,
	self.maxPatchVertices = 32,
	self.maxTessGenLevel = 64,
	self.maxViewports = 16,
	self.maxVertexAtomicCounters = 0,
	self.maxTessControlAtomicCounters = 0,
	self.maxTessEvaluationAtomicCounters = 0,
	self.maxGeometryAtomicCounters = 0,
	self.maxFragmentAtomicCounters = 8,
	self.maxCombinedAtomicCounters = 8,
	self.maxAtomicCounterBindings = 1,
	self.maxVertexAtomicCounterBuffers = 0,
	self.maxTessControlAtomicCounterBuffers = 0,
	self.maxTessEvaluationAtomicCounterBuffers = 0,
	self.maxGeometryAtomicCounterBuffers = 0,
	self.maxFragmentAtomicCounterBuffers = 1,
	self.maxCombinedAtomicCounterBuffers = 1,
	self.maxAtomicCounterBufferSize = 16384,
	self.maxTransformFeedbackBuffers = 4,
	self.maxTransformFeedbackInterleavedComponents = 64,
	self.maxCullDistances = 8,
	self.maxCombinedClipAndCullDistances = 8,
	self.maxSamples = 4,
	self.limits.nonInductiveForLoops = 1;
	self.limits.whileLoops = 1;
	self.limits.doWhileLoops = 1;
	self.limits.generalUniformIndexing = 1;
	self.limits.generalAttributeMatrixVectorIndexing = 1;
	self.limits.generalVaryingIndexing = 1;
	self.limits.generalSamplerIndexing = 1;
	self.limits.generalVariableIndexing = 1;
	self.limits.generalConstantMatrixVectorIndexing = 1;
	return self;
}

inline static EShLanguage
_map_type(BRK_SHADER_TYPE type)
{
	switch(type)
	{
		case BRK_SHADER_VERTEX:
			return EShLangVertex;

		case BRK_SHADER_PIXEL:
			return EShLangFragment;

		case BRK_SHADER_NONE:
		default:
			assert(false && "unreachable");
			return EShLangCount;
	}
}

Brk_Bytes
brk_dx11_shader_compile(const char* str, BRK_SHADER_TYPE type)
{
	static bool glsl_initialized = false;
	if(glsl_initialized == false)
	{
		bool res = glslang::InitializeProcess();
		assert(res && "glslang::InitializeProcess failed");
		glsl_initialized = true;
	}

	EShLanguage shader_type = _map_type(type);
	EShMessages messages = (EShMessages)(EShMsgSpvRules | EShMsgVulkanRules);

	glslang::TShader shader(shader_type);
	{
		int semantic_version = 400;
		glslang::EShTargetClientVersion client_version = glslang::EShTargetOpenGL_450;
		glslang::EShTargetLanguageVersion target_version = glslang::EShTargetSpv_1_0;

		shader.setStrings(&str, 1);
		shader.setEnvInput(glslang::EShSourceGlsl, shader_type, glslang::EShClientOpenGL, semantic_version);
		shader.setEnvClient(glslang::EShClientOpenGL, client_version);
		shader.setEnvTarget(glslang::EShTargetSpv, target_version);

		TBuiltInResource resources = default_builtin_resources();

		if(shader.parse(&resources, 100, false, messages) == false)
		{
			printfmt_err("{}\n{}\n", shader.getInfoLog(), shader.getInfoDebugLog());
			assert(false && "shader parsing failed");
			return Brk_Bytes{};
		}
	}

	//glsl -> spirv
	std::vector<unsigned int> ir;
	{
		glslang::TProgram program;
		program.addShader(&shader);

		if(program.link(messages) == false)
		{
			printfmt_err("{}\n{}\n", shader.getInfoLog(), shader.getInfoDebugLog());
			assert(false && "shader linking failed");
			return Brk_Bytes{};
		}

		glslang::SpvOptions options;
		glslang::GlslangToSpv(*program.getIntermediate(shader_type), ir, &options);
	}

	//spriv -> hlsl
	std::string hlsl_src;
	{
		spirv_cross::CompilerHLSL hlsl(std::move(ir));
		spirv_cross::CompilerHLSL::Options options;
		options.shader_model = 500;
		hlsl.set_options(options);
		hlsl_src = hlsl.compile();
	}

	Str res = str_new();
	str_block_push(res, Block{hlsl_src.data(), hlsl_src.size()});
	return brk_buf_bytes(res);
}