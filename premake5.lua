mn = path.getabsolute("external/mn")
glslang = path.getabsolute("external/glslang")
spriv_cross = path.getabsolute("external/spirv-cross")

workspace "shaderc"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	startproject "scratch"
	defaultplatform "x64"

	group "External"
		include "external/mn/mn"
		include "external/glslang/glslang"
		include "external/glslang/SPIRV"
		include "external/glslang/hlsl"
		include "external/glslang/OGLCompilersDLL"
		include "external/spirv-cross/spirv-cross-core"
		include "external/spirv-cross/spirv-cross-glsl"
		include "external/spirv-cross/spirv-cross-hlsl"
		include "external/spirv-cross/spirv-cross-reflect"

	group ""
		include "shaderc"
		include "scratch"