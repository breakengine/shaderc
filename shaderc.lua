project "shaderc"
	language "C++"
	kind "SharedLib"

	files
	{
		"include/**.h",
		"include/**.hpp",
		"src/**.cpp"
	}

	includedirs
	{
		"include",
		"%{mn}/include",
		"%{glslang}",
		"%{spriv_cross}"
	}

	links
	{
		"mn",
		"OGLCompiler",
		"OSDependent",
		"glslang",
		"spirv",
		"hlsl",
		"spirv-cross-core",
		"spirv-cross-glsl",
		"spirv-cross-hlsl",
		"spirv-cross-reflect"
	}

	--language configuration
	warnings "Extra"
	cppdialect "c++17"
	systemversion "latest"

	--linux configuration
	filter "system:linux"
		defines { "OS_LINUX" }


	--windows configuration
	filter "system:windows"
		defines { "OS_WINDOWS" }


	--os agnostic configuration
	filter "configurations:debug"
		targetsuffix "d"
		defines
		{
			"DEBUG",
			"SHADERC_DLL"
		}
		symbols "On"

	filter "configurations:release"
		defines
		{
			"NDEBUG",
			"SHADERC_DLL"
		}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"