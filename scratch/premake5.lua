project "scratch"
	language "C++"
	kind "ConsoleApp"

	files 
	{
		"**.cpp",
		"**.h"
	}

	includedirs
	{
		"%{mn}/include",
		"../include"
	}

	links
	{
		"mn",
		"shaderc"
	}

	cppdialect "c++17"
	systemversion "latest"

	filter "system:linux"
		defines { "OS_LINUX" }

	filter "system:windows"
		defines { "OS_WINDOWS" }
