#include <mn/IO.h>

#include <break/shaderc/Compiler.h>

using namespace mn;

int main(int argc, char** argv)
{
	printfmt("Hello, World!\n");

	const char* code = R"VERTEX(
#version 400
layout(location = 0) in vec4 position;

void main()
{
	gl_Position = position;
}
)VERTEX";
	brk_dx11_shader_compile(code, BRK_SHADER_VERTEX);
	return 0;
}