#pragma once

#include "break/shaderc/Exports.h"

#include <stddef.h>

#if __cplusplus
extern "C" {
#endif

typedef enum BRK_SHADER_TYPE__ {
	BRK_SHADER_NONE,
	BRK_SHADER_VERTEX,
	BRK_SHADER_PIXEL
} BRK_SHADER_TYPE;

typedef struct Brk_Bytes__ {
	void* allocator;
	char* ptr;
	size_t count;
	size_t cap;
} Brk_Bytes;

API_SHADERC Brk_Bytes
brk_dx11_shader_compile(const char* str, BRK_SHADER_TYPE type);

#if __cplusplus
}
#endif
