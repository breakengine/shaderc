#pragma once

#include "break/shaderc/Compiler.h"

#include <mn/Buf.h>

inline static mn::Buf<char>
brk_bytes_buf(const Brk_Bytes& bytes)
{
	return mn::Buf<char> {
		(mn::Allocator)bytes.allocator,
		bytes.ptr,
		bytes.count,
		bytes.cap
	};
}

inline static Brk_Bytes
brk_buf_bytes(const mn::Buf<char>& bytes)
{
	return Brk_Bytes {
		(void*)bytes.allocator,
		bytes.ptr,
		bytes.count,
		bytes.cap
	};
}
