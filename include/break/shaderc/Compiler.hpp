#pragma once

#include "break/shaderc/Compiler.h"
#include "break/shaderc/Cpp_Utils.h"

#include <mn/Str.h>

namespace brk::shaderc
{
	inline static mn::Str
	dx11_compile(const mn::Str& src, BRK_SHADER_TYPE type)
	{
		return brk_bytes_buf(brk_dx11_shader_compile(src.ptr, type));
	}

	inline static mn::Str
	dx11_compile(const char* src, BRK_SHADER_TYPE type)
	{
		return brk_bytes_buf(brk_dx11_shader_compile(src, type));
	}
}
