#pragma once

#if defined(OS_WINDOWS)
	#if defined(SHADERC_DLL)
		#define API_SHADERC __declspec(dllexport)
	#else
		#define API_SHADERC __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_SHADERC 
#endif